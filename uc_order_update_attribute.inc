<?php

/**
 * @file
 * Implements all the cutoms functions needed to update
 * the product attribute in an order
 */

/**
 * Implementation of _uc_order_update_attribute_get_product_attributes().
 * Gives all the selected atrtibutes for the current product
 *
 * @param array $product_attributes
 *   An array of attributes of current products
 *
 * @return array $attributes
 *   An array of selected attributes of the current product
 */
function _uc_order_update_attribute_get_product_attributes($product_attributes) {
  $attributes = array();
  $product_attrib_key = array_keys($product_attributes);

  foreach ($product_attrib_key as $key) {
    while ($current_attrib = current($product_attributes[$key])) {
      $attributes[$key] = $current_attrib;
      next($product_attributes[$key]);
    }
  }

  return $attributes;
}

/**
 * Implementation of _uc_order_update_attribute_get_product_model()
 * Gives product models for each product present in order
 *
 * @param $products_list
 *   List of products present in current order
 *
 * @return $product_models
 *   List of all the product models associated with each product in order
 */
function _uc_order_update_attribute_get_product_model($products_list) {
  $product_model = array();

  foreach ($products_list as $product) {
    $product_model[$product->nid] = $product->model;
  }

  return $product_model;
}

/**
 * Implementation of _uc_order_update_attribute_get_all_attributes()
 * Gives all the attributes of the current product
 *
 * @param $product
 *   A product object
 *
 * @param $attribute_key
 *   A currently processed attribute key
 *
 * @return $attributes
 *   An array of all the attrubutes of current product
 */
function _uc_order_update_attribute_get_all_attributes($product, $attribute_key) {
  $attributes = array();
  $product_attributes = array();

  // Load the uc_attributes module if not loaded
  module_load_include('module', 'uc_attribute');

  $product_attributes = uc_product_get_attributes($product->nid);

  if (!empty($product_attributes)) {
    foreach ($product_attributes as $attribute_value) {
      if ($attribute_value->label == $attribute_key) {
        $attributes = _uc_order_update_attribute_load_product_attrib($attribute_value);
      }
    }
  }
  else {
    $attributes = array('No Product Attributes Specified.');
  }

  return $attributes;
}

/**
 * Implementation of _uc_order_update_attribute_load_product_attrib()
 *
 * @param $product_attribute
 *   An array of product's current attribute
 *
 * @return $attributes
 *   An array of all the attributes of the current product
 */
function _uc_order_update_attribute_load_product_attrib($product_attribute) {
  $attributes = array();

  if (!empty($product_attribute)) {
    foreach ($product_attribute->options as $option) {
      $attributes[$option->name] = $option->name;
    }
  }
  else {
    $attributes = array('Error in loading the Attributes');
  }

  return $attributes;
}

/**
 * Implementation of _uc_order_update_attribute()
 * Gives the updated products with the updated attributes in order
 *
 * @param $attribute
 *   An array currently selected attributes of product in order
 *
 * @param $order_id
 *   Current Order Id
 *
 * @param $post_products
 *   An array of products from the $_POST
 *
 * @return $post_products
 *   An updated products with updated attributes in order to be given back to $_POST
 */
function _uc_order_update_attribute($product_model, $attribute, $order_id, $post_products) {
  $new_post_products = array();
  $new_post_products = $post_products;

  foreach ($attribute as $attrib_key => $attrib_value) {
    // If attribute is specified
    if ($attrib_key != 'default') {
      foreach ($post_products as $key => $product) {
        if ($product['model'] == $product_model) {
          $data = unserialize($product['data']);
          foreach ($data['attributes'] as $data_key => $data_value) {
            if ($data_key == $attrib_key && $data_value[0] != $attrib_value) {
              $data_value[0] = $attrib_value;
              $data['attributes'][$data_key] = $data_value;
            }
          }
          $data = serialize($data);
        }
        $product['data'] = $data;
        unset($new_post_products[$key]);
        $new_post_products[$key] = $product;
      }
    }
    else {
      //If no attribute is specified
      unset($new_post_products);
      $new_post_products['attribute_default'] = $attrib_key;
    }
  }

  return $new_post_products;
}